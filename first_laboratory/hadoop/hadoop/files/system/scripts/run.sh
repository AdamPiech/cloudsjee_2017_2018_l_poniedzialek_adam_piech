#!/bin/bash

dsh -M -g cluster "jps"

sudo rm -rf /tmp/hadoop-vagrant
hdfs namenode -format

start-dfs.sh
start-yarn.sh
mr-jobhistory-daemon.sh start historyserver
sh jps.sh

hdfs dfs -mkdir -p /user/vagrant/input
hdfs dfs -rm -r /user/vagrant/output

hdfs dfs -put /home/vagrant/scripts/data input

hadoop jar HandFeaturesMapReduce.jar HandFeaturesWorker input output

hdfs dfs -get output /home/vagrant/output
cat /home/vagrant/output/* | wc -l

cat /home/vagrant/output/*
