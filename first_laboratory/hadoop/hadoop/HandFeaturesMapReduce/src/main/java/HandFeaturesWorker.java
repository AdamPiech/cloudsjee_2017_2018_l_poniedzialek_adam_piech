import com.google.gson.Gson;
import model.HandFeatures;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.log4j.PropertyConfigurator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import static java.lang.Math.*;

public class HandFeaturesWorker {

    public static class TokenizerMapper extends Mapper<Object, Text, Text, DoubleWritable> {

        public void map(Object key, Text value, Mapper.Context context) throws IOException, InterruptedException {
            StringTokenizer itr = new StringTokenizer(value.toString(), "\n");
            while (itr.hasMoreTokens()) {
                HandFeatures hF = new Gson().fromJson(itr.nextToken().toString(), HandFeatures.class);
                String id = hF.getSide() + "-" + hF.getSeries() + "-";
                context.write(new Text(id + "first"), new DoubleWritable(hF.getFeatures2D().getFirst()));
                context.write(new Text(id + "second"), new DoubleWritable(hF.getFeatures2D().getSecond()));
                context.write(new Text(id + "third"), new DoubleWritable(hF.getFeatures2D().getThird()));
                context.write(new Text(id + "fourth"), new DoubleWritable(hF.getFeatures2D().getFourth()));
                context.write(new Text(id + "fifth"), new DoubleWritable(hF.getFeatures2D().getFifth()));
            }
        }

    }

    public static class NormalDistributionReducer extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {

        private DoubleWritable result = new DoubleWritable();

        public void reduce(Text key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {
            List<Double> processed_values = new ArrayList<>();
            final int SAMPLE_NO = 20;

            double sum = 0.0;
            for (DoubleWritable value : values) {
                sum += value.get();
                processed_values.add(value.get());
            }
            double mean = sum / SAMPLE_NO;

            double sigma = 0.0;
            for(Double val : processed_values){
                sigma += pow(val - mean, 2);
            }
            sigma = sqrt(sigma/SAMPLE_NO);

            result.set(sigma);
            context.write(key, result);
        }
    }

    public static void main(String[] args) throws Exception {
//        PropertyConfigurator.configure(HandFeaturesWorker.class.getResource("slf4j.properties"));
        Configuration conf = new Configuration();

        Job job = Job.getInstance(conf, "HAND FEATURES WORKER");
        job.setJarByClass(HandFeaturesWorker.class);
        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(NormalDistributionReducer.class);
        job.setReducerClass(NormalDistributionReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(DoubleWritable.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }

}
