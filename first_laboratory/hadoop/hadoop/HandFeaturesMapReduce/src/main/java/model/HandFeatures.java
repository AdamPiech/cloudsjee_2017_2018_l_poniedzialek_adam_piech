package model;

public class HandFeatures {

    private long timestamp;
    private Features2D features2D;
    private String side;
    private int series;
    private int sample;

    public HandFeatures() {}

    public HandFeatures(long timestamp, Features2D features2D, String side, int series, int sample) {
        this.timestamp = timestamp;
        this.features2D = features2D;
        this.side = side;
        this.series = series;
        this.sample = sample;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public Features2D getFeatures2D() {
        return features2D;
    }

    public void setFeatures2D(Features2D features2D) {
        this.features2D = features2D;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public int getSeries() {
        return series;
    }

    public void setSeries(int series) {
        this.series = series;
    }

    public int getSample() {
        return sample;
    }

    public void setSample(int sample) {
        this.sample = sample;
    }

}
