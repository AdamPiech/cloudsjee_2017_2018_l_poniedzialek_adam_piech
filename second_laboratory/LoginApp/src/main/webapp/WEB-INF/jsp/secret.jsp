<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Logged</title>
    </head>
    <body>
        <div>
            <h3>
                Logged as: <span th:text="${user.getLogin()}"></span>!
            </h3>
            <h5>
                Hadoop output:
            </h5>

            <c:forEach items="${output}" var="line">
                ${line}<br>
            </c:forEach>
        </div>
        <a href="/logout">Logout</a>
    </body>
</html>

