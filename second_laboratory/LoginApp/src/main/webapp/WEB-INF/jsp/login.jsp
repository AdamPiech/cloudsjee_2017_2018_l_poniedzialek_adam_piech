<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns:th="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>Login</title>
    </head>
    <body>
        <div>
            <form method="post">
                <p th:if="${invalidCredentials}">
                    Invalid login or password!
                </p>
                <input type="text" name="login" placeholder="login" /><br />
                <input type="password" name="password" placeholder="password" /><br />
                <button type="submit">Login</button>
            </form>
        </div>
    </body>
</html>