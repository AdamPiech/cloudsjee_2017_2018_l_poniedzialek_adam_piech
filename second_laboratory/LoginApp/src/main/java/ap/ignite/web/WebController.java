package ap.ignite.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import ap.ignite.web.api.utils.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ap.ignite.model.User;
import ap.ignite.model.UserList;

@Controller
public class WebController {

    private static final String USER_SESSION_VALUE = "user";
    private static final String OUTPUT_SESSION_VALUE = "user";

    private final UserList userList;

    @Autowired
    public WebController(UserList userList) {
        this.userList = userList;
    }

    @GetMapping("/login")
    public String loginGet(HttpServletRequest request, Model model) {
        Object user = request.getSession().getAttribute(USER_SESSION_VALUE);
        if (user != null) {
            model.addAttribute("user", user);
            WebUtil.downloadHadoopOutput(string ->
                    model.addAttribute("output", new ArrayList<>(Arrays.asList(string.split("\n")))));
            return "secret";
        }
        return "login";
    }

    @PostMapping("/login")
    public String loginPost(@RequestParam("login") String username, @RequestParam("password") String password,
            HttpServletRequest request, Model model) throws InterruptedException {
        Optional<User> findOne = userList.findOne(username);
        if (findOne.isPresent()) {
            User user = findOne.get();
            if (user.getPassword().equals(password)) {
                request.getSession().setAttribute(USER_SESSION_VALUE, user);
                model.addAttribute("user", findOne.get());
                WebUtil.downloadHadoopOutput(string ->
                        model.addAttribute("output", new ArrayList<>(Arrays.asList(string.split("\n")))));
//                WebUtil.downloadHadoopOutput(string -> {
//                    request.getSession().setAttribute(OUTPUT_SESSION_VALUE, string);
//                    model.addAttribute("output", new ArrayList<>(Arrays.asList(string.split("\n"))));
//                });
                return "secret";
            }
        }
        model.addAttribute("invalidCredentials", true);
        return "login";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request) {
        request.getSession().removeAttribute(USER_SESSION_VALUE);
        return "redirect:/login";
    }
}