package ap.ignite.web.api.utils;

import ap.ignite.web.api.WebAPI;
import ap.ignite.web.api.utils.callback.ICallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class WebUtil {

    public static void downloadHadoopOutput(ICallback callback) {
        Call<String> output = initWebAPIClient().getHadoopOutput();
        output.enqueue(
                new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {
                            callback.setResult(response.body());
                        }
                    }
                    @Override
                    public void onFailure(Call<String> call, Throwable throwable) {}
                }
        );
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private static WebAPI initWebAPIClient() {
        return new Retrofit.Builder()
                .baseUrl("http://192.168.5.11:50075")
                .addConverterFactory(ScalarsConverterFactory.create())
                .build()
                .create(WebAPI.class);
    }

}
