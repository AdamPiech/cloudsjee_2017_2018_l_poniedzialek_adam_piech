package ap.ignite.web.api;

import retrofit2.Call;
import retrofit2.http.GET;

public interface WebAPI {

        @GET("webhdfs/v1/user/vagrant/output/part-r-00000?op=OPEN&user.name=vagrant&namenoderpcaddress=hadoop-master:8020&offset=0")
        Call<String> getHadoopOutput();

}
