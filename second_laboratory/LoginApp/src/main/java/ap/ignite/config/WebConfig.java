package ap.ignite.config;

import javax.servlet.ServletContextListener;

import org.apache.ignite.cache.websession.WebSessionFilter;
import org.apache.ignite.startup.servlet.ServletContextListenerStartup;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@Configuration
public class WebConfig {

    public static String IGNITE = "ignite-config.xml";
    public static String CACHE = "cache";

//    @Value("${spring.mvc.view.prefix}")
//    private String prefix;
//
//    @Value("${spring.mvc.view.suffix}")
//    private String suffix;
//
//    @Bean
//    public InternalResourceViewResolver setupViewResolver() {
//        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
//        resolver.setPrefix(prefix);
//        resolver.setSuffix(suffix);
//        resolver.setViewClass(JstlView.class);
//        return resolver;
//    }

//    @Bean
//    public ViewResolver jspViewResolver() {
//        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
//        resolver.setPrefix("/WEB-INF/jsp/");
//        resolver.setSuffix(".jsp");
//        return resolver;
//    }

    @Bean
    public ServletContextListener servletContextListener() {
        return new ServletContextListenerStartup();
    }

    @Bean
    public ServletContextInitializer servletContextInitializer() {
        return servletContext -> {
            servletContext.setInitParameter("IgniteConfigurationFilePath", IGNITE);
            servletContext.setInitParameter("IgniteWebSessionsCacheName", CACHE);
        };
    }

    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setName("IgniteWebSessionsFilter");
        filterRegistrationBean.setFilter(new WebSessionFilter());
        filterRegistrationBean.addUrlPatterns("/*");
        return filterRegistrationBean;
    }

}