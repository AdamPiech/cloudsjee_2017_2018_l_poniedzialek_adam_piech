package ap.ignite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class Main {

	public static void main(String[] args) {
		SpringApplication springApplication = new SpringApplication(Main.class);
		springApplication.addListeners(new ApplicationPidFileWriter("pid_boot_app"));
		springApplication.run(args);
	}

}

//@SpringBootApplication
//public class Main extends SpringBootServletInitializer {
//
//	@Override
//	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//		return application.sources(Main.class);
//	}
//
//	public static void main(String[] args) throws Exception {
//		SpringApplication.run(Main.class, args);
//	}
//}
